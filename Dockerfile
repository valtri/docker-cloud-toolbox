FROM debian:bookworm
LABEL maintainer "František Dvořák <valtri@civ.zcu.cz>"

SHELL [ "/bin/bash", "-o", "pipefail", "-c" ]

RUN apt-get update && apt-get install -y --no-install-recommends \
    curl \
    git \
    gnupg \
    iputils-ping \
    jq \
    openssh-client \
    python3-flake8 \
    python3-hvac \
    python3-openstackclient \
    rsync \
    sudo \
    unzip \
 && rm -rf /var/lib/apt/lists/*

# https://learn.hashicorp.com/tutorials/terraform/install-cli#install-terraform
RUN curl -fsSL https://apt.releases.hashicorp.com/gpg | gpg --dearmor > /usr/share/keyrings/hashicorp-archive-keyring.gpg \
 && echo "deb [arch=amd64 signed-by=/usr/share/keyrings/hashicorp-archive-keyring.gpg] https://apt.releases.hashicorp.com bookworm main" > /etc/apt/sources.list.d/hashicorp.list \
 && apt-get update \
 && apt-get install -y --no-install-recommends terraform vault \
 && rm -rf /var/lib/apt/lists/*

# required by vault client
RUN setcap cap_ipc_lock= /usr/bin/vault

# https://docs.ansible.com/ansible/latest/installation_guide/intro_installation.html (unused)
RUN apt-get update && apt-get install -y --no-install-recommends \
    ansible \
 && rm -rf /var/lib/apt/lists/*

# https://github.com/adammck/terraform-inventory#installation
RUN curl -fsSL https://github.com/adammck/terraform-inventory/releases/download/v0.10/terraform-inventory_v0.10_linux_amd64.zip > terraform-inventory.zip \
 && unzip terraform-inventory*.zip \
 && mv terraform-inventory /usr/local/bin \
 && rm -f terraform-inventory*.zip

RUN useradd -ms /bin/bash cloud \
 && printf "cloud\tALL=(ALL)\tNOPASSWD:ALL\n" > /etc/sudoers.d/cloud
USER cloud
WORKDIR /home/cloud

HEALTHCHECK CMD true
