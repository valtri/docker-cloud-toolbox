# Cloud toolbox image

* [Ansible](https://www.ansible.com/)
* [OpenStack client](https://docs.openstack.org/python-openstackclient/)
* [Terraform](https://www.terraform.io/)
* [Terraform Inventory](https://github.com/adammck/terraform-inventory)
